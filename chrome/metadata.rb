name             "chrome"
maintainer       "David Kortleven"
maintainer_email "david@startmetplate.nl"
license          "Apache 2.0"
description      "Installs/Configures chrome"
version          "0.0.1"

%w{fedora centos rhel ubuntu debian}.each do |os|
  supports os
end
