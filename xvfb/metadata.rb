name             "xvfb"
maintainer       "David Kortleven"
maintainer_email "david@startmetplate.nl"
license          "Apache 2.0"
description      "Installs/Configures xvfb"
version          "0.0.1"


supports 'debian'
supports 'centos'
supports 'fedora'
supports 'redhat'
supports 'ubuntu'
