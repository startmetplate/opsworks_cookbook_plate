#
# Cookbook:: plate
# Recipe:: cronjobs
#
# Copyright:: 2019, The Authors, All Rights Reserved.


cron_env = {"PATH" => "/usr/local/bin"}

Chef::Log.info("Install crontab 'upload rails logs to S3'")
cron "upload rails logs to S3" do
  environment cron_env
  hour "3"
  minute "0"
  user 'deploy'
  command "cd /srv/www/plate/current && /usr/local/bin/bundle exec rake logs:upload_to_s3 RAILS_ENV=production"
end