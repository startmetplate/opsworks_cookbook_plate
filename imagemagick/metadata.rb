name             "imagemagick"
maintainer       "Kobus Post"
maintainer_email "kobus@startmetplate.nl"
license          "Apache 2.0"
description      "Installs/Configures imagemagick"
version          "0.0.1"

recipe "imagemagick", "Installs imagemagick package"
recipe "imagemagick::devel", "Installs imagemagick development libraries"
recipe "imagemagick::rmagick", "Installs rmagick gem"

%w{fedora centos rhel ubuntu debian}.each do |os|
  supports os
end
